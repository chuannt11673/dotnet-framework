﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Ch.Core.Swagger
{
    public static class SwaggerExtension
    {
        public static IServiceCollection InitSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "Ch API", Version = "v1" });
            });
            return services;
        }

        public static IApplicationBuilder UseInitSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.RoutePrefix = "";
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Ch API V1");
            });
            return app;
        }
    }
}
