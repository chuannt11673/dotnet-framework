﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Ch.Core.Attributes
{
    public class Inject : Attribute
    {
        public ServiceLifetime ServiceLifetime { get; set; }
        public Type ServiceType { get; set; }
        public Inject(Type serviceType, ServiceLifetime serviceLifetime = ServiceLifetime.Scoped)
        {
            ServiceType = serviceType;
            ServiceLifetime = serviceLifetime;
        }
    }
}
