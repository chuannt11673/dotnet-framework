﻿using Ch.Core.Attributes;
using Ch.Core.Constants;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Ch.Core.Injection
{
    /// <summary>
    ///     Inject scanner
    /// </summary>
    public static class Scanner
    {
        public static void Inject(IServiceCollection services, string assemblyDll = "*")
        {
            var path = $"{ApplicationSetting.DebugDirectory}\\{assemblyDll}";
            Assembly assembly = Assembly.LoadFrom(path);
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                var customAttribute = type.GetCustomAttribute<Inject>();
                if(customAttribute != null)
                {
                    switch (customAttribute.ServiceLifetime)
                    {
                        case Microsoft.Extensions.DependencyInjection.ServiceLifetime.Singleton:
                            services.AddSingleton(customAttribute.ServiceType, type);
                            break;
                        case Microsoft.Extensions.DependencyInjection.ServiceLifetime.Scoped:
                            services.AddScoped(customAttribute.ServiceType, type);
                            break;
                        case Microsoft.Extensions.DependencyInjection.ServiceLifetime.Transient:
                            services.AddTransient(customAttribute.ServiceType, type);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
