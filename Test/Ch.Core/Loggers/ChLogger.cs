﻿using Serilog;

namespace Ch.Core.Loggers
{
    public static class ChLogger
    {
        public static ILogger InfoLogger => GetLogger("Logs/info.txt");
        public static ILogger WarningLogger => GetLogger("Logs/warning.txt");
        public static ILogger ErrorLogger => GetLogger("Logs/error.txt");

        /// <summary>
        /// Create logger with fileName (file name must be included extension)
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static ILogger GetLogger(string fileName)
        {
            return new LoggerConfiguration()
                .WriteTo.File($"{fileName}")
                .CreateLogger();
        }
    }
}
