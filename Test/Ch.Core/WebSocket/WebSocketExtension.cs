﻿using Ch.Core.Loggers;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Ch.Core.WebSocket
{
    public static class WebSocketExtension
    {
        public static IServiceCollection InitWebSocket(this IServiceCollection services)
        {
            return services;
        }

        public static IApplicationBuilder UseInitWebSocket(this IApplicationBuilder app)
        {
            app.UseWebSockets(new WebSocketOptions
            {
                KeepAliveInterval = TimeSpan.FromSeconds(120),
                ReceiveBufferSize = 4 * 1024
            });

            app.Use(async (context, next) =>
            {
                if(context.Request.Path == "/ws")
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        var webSocket = await context.WebSockets.AcceptWebSocketAsync();
                        ChLogger.InfoLogger.Information("Accept websocket");
                    }
                    else
                    {
                        context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                    }
                }
                else
                {
                    await next();
                }
            });
            return app;
        }
    }
}
