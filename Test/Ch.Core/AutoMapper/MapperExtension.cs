﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Ch.Core.AutoMapper
{
    public static class MapperExtension
    {
        public static IServiceCollection InitAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper();
            return services;
        }
    }
}
