﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ch.Core.Infrastructure
{
    public class ApplicationResult
    {
        public bool IsSuccess { get; set; }
        public List<string> Errors { get; set; }

        protected ApplicationResult(bool isSuccess, List<string> errors)
        {
            IsSuccess = isSuccess;
            Errors = errors;
        }

        public static ApplicationResult Ok()
        {
            return new ApplicationResult(true, new List<string>());
        }

        public static ApplicationResult<T> Ok<T>(T value)
        {
            return new ApplicationResult<T>(value);
        }

        public static ApplicationResult Fail(string error)
        {
            return new ApplicationResult(false, new List<string> { error });
        }

        public static ApplicationResult Fail(List<string> errors)
        {
            return new ApplicationResult(false, errors);
        }
    }

    public class ApplicationResult<T> : ApplicationResult
    {
        public T Value { get; set; }

        public ApplicationResult(T value) : base(true, new List<string>())
        {
            Value = value;
        }
        public ApplicationResult(T value, bool isSuccess, List<string> errors) : base(isSuccess, errors)
        {
            Value = value;
        }
    }
}
