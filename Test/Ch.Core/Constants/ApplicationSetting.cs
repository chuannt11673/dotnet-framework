﻿using Microsoft.Extensions.Configuration;
using System.IO;
using System.Reflection;

namespace Ch.Core.Constants
{
    public static class ApplicationConsts
    {
    }
    public static class ApplicationSetting
    {
        public static string DebugDirectory => Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        public static string ConnectionString { get; set; }
        public static string JwtSecret { get; set; }

    }

    public static class ApplicationSettingExtensions
    {
        public static void InitAppSetting(this IConfiguration configuration)
        {
            ApplicationSetting.ConnectionString = configuration.GetConnectionString("DefaultConnection");
            ApplicationSetting.JwtSecret = configuration.GetSection("Appsettings")["Secret"];
        }
    }
}
