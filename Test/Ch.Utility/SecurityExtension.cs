﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Ch.Utility
{
    public static class SecurityExtension
    {
        public static string Encrypt(this string content)
        {
            var result = new StringBuilder();
            var md5 = MD5.Create();
            var bytes = Encoding.ASCII.GetBytes(content);
            var hash = md5.ComputeHash(bytes);
            foreach (var value in hash)
            {
                result.Append(value.ToString("X2"));
            }
            return result.ToString();
        }
    }
}
