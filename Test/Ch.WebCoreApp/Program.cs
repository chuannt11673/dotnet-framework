﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ch.Core.Loggers;
using Ch.EF;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Ch.WebCoreApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args)
                .ConfigureLogging((hostingContext, builder) => {
                    builder.AddFile("Logs/logs-{Date}.txt");
                    })
                .Build()
                .Logger()
                .MigrateDb()
                .Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();        
    }

    public static class Init
    {
        public static IWebHost Logger(this IWebHost host)
        {
            ChLogger.InfoLogger.Information("Run application");
            return host;
        }
        public static IWebHost MigrateDb(this IWebHost host)
        {
            var serviceScope = (IServiceScopeFactory) host.Services.GetService(typeof(IServiceScopeFactory));
            using(var scope = serviceScope.CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;
                var dbContext = serviceProvider.GetRequiredService<ApplicationDbContext>();
                dbContext.Database.Migrate();
                return host;
            }
        }
    }
}
