﻿using Ch.Service.IServices;
using Ch.Service.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Ch.WebCoreApp.Controllers.Client
{
    [Route("api/user")]
    [ApiController]
    public class UserApiController : BaseApiController
    {
        private readonly IUserService _userService;
        public UserApiController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [Route("create")]
        [HttpPost]
        public IActionResult Create([FromBody] UserCreateViewModel model)
        {
            return Result(() =>
            {
                var res = _userService.Create(model);
                return res;
            });
        }
    }
}