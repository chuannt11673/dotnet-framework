﻿using Ch.Core.Infrastructure;
using Ch.Core.Loggers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Ch.WebCoreApp.Controllers
{
    public class BaseApiController : Controller
    {
        protected IActionResult Result(Func<ApplicationResult> func)
        {
            try
            {
                var result = func();
                return Ok(result);
            }
            catch(Exception ex)
            {
                return Ok(ApplicationResult.Fail(ex.Message));
            }
        }

        protected async Task<IActionResult> Result(Func<Task<ApplicationResult>> funcAsync)
        {
            try
            {
                var result = await funcAsync();
                return Ok(result);
            }
            catch(Exception ex)
            {
                return Ok(ApplicationResult.Fail(ex.Message));
            }
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var cancellationToken = context.HttpContext?.RequestAborted ?? CancellationToken.None;
            if (cancellationToken.IsCancellationRequested)
            {
                ChLogger.ErrorLogger.Error("Request was cancelled.");
                context.Result = new OkObjectResult(ApplicationResult.Fail("Request was cancelled."));
                return;
            }
            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            var cancellationToken = context.HttpContext?.RequestAborted ?? CancellationToken.None;
            if (cancellationToken.IsCancellationRequested)
            {
                ChLogger.ErrorLogger.Error("Request was cancelled.");
                context.Result = new OkObjectResult(ApplicationResult.Fail("Request was cancelled."));
                return;
            }
            base.OnActionExecuted(context);
        }
    }
}