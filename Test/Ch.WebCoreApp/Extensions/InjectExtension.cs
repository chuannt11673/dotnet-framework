﻿using Ch.Core.Injection;
using Ch.Service;
using Ch.Service.IServices;
using Microsoft.Extensions.DependencyInjection;

namespace Ch.WebCoreApp.Extensions
{
    public static class InjectExtension
    {
        public static IServiceCollection AddInjection(this IServiceCollection services)
        {
            Scanner.Inject(services, "Ch.Repo.dll");
            Scanner.Inject(services, "Ch.Service.dll");
            return services;
        }
    }
}
