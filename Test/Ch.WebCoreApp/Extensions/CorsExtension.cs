﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ch.WebCoreApp.Extensions
{
    public static class CorsExtension
    {
        public static IApplicationBuilder SetupCors(this IApplicationBuilder app)
        {
            app.UseCors(x => x
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());
            return app;
        }
    }
}
