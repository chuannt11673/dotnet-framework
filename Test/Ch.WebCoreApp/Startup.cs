﻿using Ch.Core.AutoMapper;
using Ch.Core.Constants;
using Ch.Core.Swagger;
using Ch.EF;
using Ch.WebCoreApp.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Ch.WebCoreApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private readonly IConfiguration _configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //  init app setting
            _configuration.InitAppSetting();
            services.AddDbContext<ApplicationDbContext>();

            services
                .AddJwtAuthenticate()
                .AddInjection()
                .InitAutoMapper()
                .Configure<ApiBehaviorOptions>(x =>
                {
                    x.SuppressModelStateInvalidFilter = true;
                });
            services.AddMvc();

            services.InitSwagger();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app
                .UseStaticFiles()
                .UseAuthentication()
                .SetupCors()
                .UseMvc();

            app
                .UseInitSwagger();
            app.Run(async (context) => { 
                await context.Response.WriteAsync("Hello");
                });
        }
    }
}
