﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var store = new Store();
            var cars = store.Cars;

            foreach (var car in cars)
            {
                car.Price = 100000;
            }

            Console.WriteLine($"Cars in store: {JsonConvert.SerializeObject(store.Cars.Select(x => x.Price).ToList())}");
            Console.WriteLine($"Cars object: {JsonConvert.SerializeObject(cars.Select(x => x.Price).ToList())}");

            Console.ReadKey();
        }


    }

    class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Currency Currency { get; set; }
    }

    class Lambogini : Car
    {
        public Lambogini(int id)
        {
            Id = id;
            Name = "Lambo";
            Price = 500000;
            Currency = Currency.Dolar;
        }
    }

    class Store
    {
        public List<Car> Cars { get; set; }

        public Store()
        {
            Cars = new List<Car>
            {
                new Lambogini(1),
                new Lambogini(2),
                new Lambogini(3),
                new Lambogini(4),
                new Lambogini(5),
                new Lambogini(6)
            };
        }
    }

    enum Currency
    {
        VietNamDong,
        Dolar
    }
}
