﻿using Ch.EF;

namespace Ch.Repo.IRepo
{
    public interface IBaseRepo<TEntity> : IRepository<BaseEntity, int> 
    {
    }
}
