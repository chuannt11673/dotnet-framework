﻿using Ch.EF.Entities;

namespace Ch.Repo.IRepo
{
    public interface IUserRepo : IRepository<User, string>
    {
    }
}
