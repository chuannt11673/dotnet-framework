﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Ch.Repo.IRepo
{
    public interface IRepository<TEntity, TKey> where TEntity : Ch.EF.TEntity<TKey>, new()
    {
        TEntity Create(TEntity entity);
        void Create(List<TEntity> entities);

        void Update(TEntity entity, params Expression<Func<TEntity, object>>[] properties);
        void Update(List<TEntity> entities, params Expression<Func<TEntity, object>>[] properties);

        void Delete(TKey id);
        void Delete(Expression<Func<TEntity, bool>> condition);

        TEntity GetById(TKey id);
        TEntity Get(Expression<Func<TEntity, bool>> condition);
        List<TEntity> Gets(Expression<Func<TEntity, bool>> condition);
    }
}
