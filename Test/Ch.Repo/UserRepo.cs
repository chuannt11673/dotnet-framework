﻿using Ch.Core.Attributes;
using Ch.EF.Entities;
using Ch.Repo.IRepo;

namespace Ch.Repo
{
    [Inject(typeof(IUserRepo))]
    public class UserRepo : Repository<User, string>, IUserRepo
    {
    }
}
