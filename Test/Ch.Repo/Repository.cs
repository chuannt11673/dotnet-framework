﻿using Ch.EF;
using Ch.Repo.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Ch.Repo
{
    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : class, TEntity<TKey>, new()
    {
        public TEntity Create(TEntity entity)
        {
            using(var db = new ApplicationDbContext())
            {
                var dbSet = db.Set<TEntity>();
                var rsl = dbSet.Add(entity);
                db.SaveChanges();
                return rsl.Entity;
            }
        }

        public void Create(List<TEntity> entities)
        {
            using(var db = new ApplicationDbContext())
            {
                var dbSet = db.Set<TEntity>();
                dbSet.AddRange(entities);
                db.SaveChanges();
                return;
            }
        }

        public void Delete(TKey id)
        {
            using(var db = new ApplicationDbContext())
            {
                var dbSet = db.Set<TEntity>();
                var entity = dbSet.Find(id);
                if(entity == null)
                    return;

                entity.TimeDeletedOffset = DateTimeOffset.UtcNow;
                db.SaveChanges();
                return;
            }
        }

        public void Delete(Expression<Func<TEntity, bool>> condition)
        {
            using(var db = new ApplicationDbContext())
            {
                var dbSet = db.Set<TEntity>();
                dbSet.RemoveRange(dbSet.Where(condition));
                db.SaveChanges();
                return;
            }
        }

        public TEntity Get(Expression<Func<TEntity, bool>> condition)
        {
            using(var db = new ApplicationDbContext())
            {
                var dbSet = db.Set<TEntity>();
                var entity = dbSet.FirstOrDefault(condition);
                return entity;
            }
        }

        public TEntity GetById(TKey id)
        {
            using(var db = new ApplicationDbContext())
            {
                var dbSet = db.Set<TEntity>();
                var entity = dbSet.FirstOrDefault(x => x.Id.Equals(id));
                return entity;
            }
        }

        public List<TEntity> Gets(Expression<Func<TEntity, bool>> condition)
        {
            using (var db = new ApplicationDbContext())
            {
                var dbSet = db.Set<TEntity>();
                var entities = dbSet.Where(condition).ToList();
                return entities;
            }
        }

        public void Update(TEntity entity, params Expression<Func<TEntity, object>>[] properties)
        {
            using(var db = new ApplicationDbContext())
            {
                foreach (var property in properties)
                {
                    db.Entry(entity).Property(property).IsModified = true;
                }
                entity.TimeModifiedOffset = DateTimeOffset.UtcNow;
                db.SaveChanges();
                return;
            }
        }

        public void Update(List<TEntity> entities, params Expression<Func<TEntity, object>>[] properties)
        {
            using(var db = new ApplicationDbContext())
            {
                foreach (var entity in entities)
                {
                    entity.TimeModifiedOffset = DateTimeOffset.UtcNow;
                    foreach (var property in properties)
                    {
                        db.Entry(entity).Property(property).IsModified = true;
                    }
                }
                db.SaveChanges();
                return;
            }
        }
    }
}
