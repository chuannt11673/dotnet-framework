﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Ch.EF
{
    public class BaseEntity : TEntity<int>
    {
        public BaseEntity()
        {
            TimeCreatedOffset = DateTimeOffset.UtcNow;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public DateTimeOffset TimeCreatedOffset { get; set; }
        public DateTimeOffset? TimeModifiedOffset { get; set; }
        public DateTimeOffset? TimeDeletedOffset { get; set; }
    }
}
