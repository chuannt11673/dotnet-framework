﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ch.EF.Entities
{
    public class User : TEntity<string>
    {
        public User()
        {
            TimeCreatedOffset = DateTimeOffset.UtcNow;
        }

        public string Id { get; set; }
        public DateTimeOffset TimeCreatedOffset { get; set; }
        public DateTimeOffset? TimeModifiedOffset { get; set; }
        public DateTimeOffset? TimeDeletedOffset { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

    }
}
