﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ch.EF.Entities
{
    public class Land : BaseEntity
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
