﻿using Ch.Core.Constants;
using Ch.EF.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;


namespace Ch.EF
{
    public class ApplicationDbContext : DbContext
    {
        public ILoggerFactory ILoggerFactory { get; set; }
        public ApplicationDbContext()
        {
        }

        public DbSet<Land> Lands { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLoggerFactory(ILoggerFactory);
            optionsBuilder.UseSqlServer(ApplicationSetting.ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
