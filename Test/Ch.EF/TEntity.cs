﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ch.EF
{
    public interface TEntity<TKey>
    {
        [Key]
        TKey Id { get; set; }
        DateTimeOffset TimeCreatedOffset { get; set; }
        DateTimeOffset? TimeModifiedOffset { get; set; }
        DateTimeOffset? TimeDeletedOffset { get; set; }
    }
}
