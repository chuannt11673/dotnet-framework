﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ch.EF.Migrations
{
    public partial class AddUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    TimeCreatedOffset = table.Column<DateTimeOffset>(nullable: false),
                    TimeModifiedOffset = table.Column<DateTimeOffset>(nullable: false),
                    TimeDeletedOffset = table.Column<DateTimeOffset>(nullable: false),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
