﻿
using System;
using Ch.Core.Infrastructure;
using Ch.Service.Models;

namespace Ch.Service.IServices
{
    public interface IUserService
    {
        ApplicationResult Create(UserCreateViewModel model);
    }
}
