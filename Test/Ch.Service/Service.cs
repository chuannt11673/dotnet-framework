﻿using AutoMapper;
using Ch.Service.IServices;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ch.Service
{
    public class Service : IService
    {
        protected readonly IMapper _mapper;
        public Service(IMapper mapper)
        {
            _mapper = mapper;
        }
    }
}
