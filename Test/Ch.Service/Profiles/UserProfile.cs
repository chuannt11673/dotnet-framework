﻿using AutoMapper;
using Ch.EF.Entities;
using Ch.Service.Models;

namespace Ch.Service.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserCreateViewModel, User>()
                .ForMember(x => x.Username, opt => opt.MapFrom(s => s.Username.ToLower()))
                .ForMember(x => x.Password, opt => opt.Ignore());
        }
    }
}
