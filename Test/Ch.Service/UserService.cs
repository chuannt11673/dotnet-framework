﻿using AutoMapper;
using Ch.Core.Attributes;
using Ch.Core.Infrastructure;
using Ch.EF.Entities;
using Ch.Repo.IRepo;
using Ch.Service.IServices;
using Ch.Service.Models;
using Ch.Utility;

namespace Ch.Service
{
    [Inject(typeof(IUserService))]
    public class UserService : Service, IUserService
    {
        private readonly IUserRepo _userRepo;
        public UserService(IMapper mapper, IUserRepo userRepo) : base(mapper)
        {
            _userRepo = userRepo;
        }

        public ApplicationResult Create(UserCreateViewModel model)
        {
            var user = _mapper.Map<User>(model);
            if (_userRepo.Get(x => x.Username == user.Username) != null)
            {
                return ApplicationResult.Fail("Username is already existed");
            }

            SetPassword(user, model.Password);
            var res = _userRepo.Create(user);
            return ApplicationResult.Ok();
        }

        private void SetPassword(User user, string password)
        {
            user.Password = password.Encrypt();
        }
    }
}
